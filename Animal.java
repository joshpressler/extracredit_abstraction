
/*
 * Abstraction allows programmers to program generally.  Using abstraction allows only the essential features of an object to be placed in the abstract class.
 * Classes extending that class are then required to have those features, and can add any extras that are needed for that specific class. 
 */
public abstract class Animal {
	
	public String name;						//the name of the specific animal
	
	public String habitat;					//the habitat of the specific animal
	
	int birthAge = 0;						//all animals born at age 0

}
