
public class Dog extends Animal{
	
	public Dog(){
		
		name = "Dog";							//the animal is a dog
		habitat = "In a loving home";			//a dog's habitat
	}

}
